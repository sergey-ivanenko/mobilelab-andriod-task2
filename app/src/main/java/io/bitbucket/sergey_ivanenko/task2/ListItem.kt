package io.bitbucket.sergey_ivanenko.task2

data class ListItem(
    val title: String = "",
    val description: String = ""
)