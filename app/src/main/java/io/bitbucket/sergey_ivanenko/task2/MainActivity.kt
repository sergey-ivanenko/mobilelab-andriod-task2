package io.bitbucket.sergey_ivanenko.task2

import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import io.bitbucket.sergey_ivanenko.task2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), Navigator {

    private var _binding: ActivityMainBinding? = null
    private val binding get() = requireNotNull(_binding)

    private val currentFragment: Fragment?
        get() = supportFragmentManager.findFragmentById(R.id.fragmentContainer)

    private val fragmentListener = object : FragmentManager.FragmentLifecycleCallbacks() {
        override fun onFragmentViewCreated(
            fragmentManager: FragmentManager,
            fragment: Fragment,
            view: View,
            savedInstanceState: Bundle?,
        ) {
            super.onFragmentViewCreated(fragmentManager, fragment, view, savedInstanceState)
            updateUi()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragmentContainer, ListFragment())
                .commit()
        }

        supportFragmentManager.registerFragmentLifecycleCallbacks(fragmentListener, false)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()

        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        updateUi()

        return true
    }

    override fun showDetails(title: String, description: String) {
        supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .replace(R.id.fragmentContainer, DetailsFragment.newInstance(title, description))
            .commit()
    }

    private fun updateUi() {
        when (currentFragment) {
            is ListFragment -> {
                binding.toolbar.apply {
                    title = getString(R.string.items)
                    navigationIcon = null
                }

            }
            is DetailsFragment -> {
                binding.toolbar.apply {
                    title = getString(R.string.detail)
                    navigationIcon =
                        ContextCompat.getDrawable(
                            this@MainActivity,
                            R.drawable.arrow_back_24)?.let { DrawableCompat.wrap(it) }

                }
            }
        }
    }

    override fun goBack() {
        onBackPressed()
    }

    override fun closeApp() {
        this.moveTaskToBack(true)
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }
}