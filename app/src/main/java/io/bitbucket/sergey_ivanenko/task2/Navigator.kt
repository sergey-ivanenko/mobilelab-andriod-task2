package io.bitbucket.sergey_ivanenko.task2

interface Navigator {
    fun showDetails(title: String, description: String)
    fun goBack()
    fun closeApp()
}