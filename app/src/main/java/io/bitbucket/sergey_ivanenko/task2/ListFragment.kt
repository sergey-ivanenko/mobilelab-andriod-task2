package io.bitbucket.sergey_ivanenko.task2

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import io.bitbucket.sergey_ivanenko.task2.databinding.FragmentListBinding

class ListFragment : Fragment(R.layout.fragment_list),
    ListAdapter.OnItemClickListener {

    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding get() = requireNotNull(_binding)

    private var navigator: Navigator? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        //navigator = activity as Navigator
        //navigator = context as? Navigator
        if (context is Navigator) {
            navigator = context
        } else {
            throw RuntimeException("$context must implement Navigator")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentListBinding.bind(view)

        val adapter = ListAdapter(generateList(), this)

        binding.apply {
            recyclerView.adapter = adapter
            recyclerView.setHasFixedSize(true)
            recyclerView.itemAnimator = null
        }
    }

    override fun onItemClick(item: ListItem) {
        navigator?.showDetails(
            title = item.title,
            description = item.description
        )
    }

    private fun generateList(): List<ListItem> {
        val list = mutableListOf<ListItem>()
        for (item in 1..1000) {
            list.add(ListItem("Title $item", "Description $item"))
        }

        return list
    }

    override fun onDetach() {
        super.onDetach()
        navigator = null
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}